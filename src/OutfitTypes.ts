import { Calculate, CompareColor, CompareRecord, IsStringArray, Result } from "./TypeSupport";

export type OutfitClothType = {
    Asset: {
        Name: string;
        Group: AssetGroupBodyName;
    };
    Color: Calculate<ItemColor>;
    Property?: {
        TypeRecord?: TypeRecord;
        OverridePriority?: number | { [key: string]: number };
    };
};

type ExtendedItemProperties = ItemProperties & {
    ExcludeEffect?: string[];
}

export type OutfitItemType = {
    Asset: {
        Name: string,
        Group: AssetGroupItemName,
        OverrideGroupDescription?: Calculate<string>,
    };
    Color: Calculate<ItemColor>;
    Craft: {
        Name: string;
        Description: string;
        Lock?: AssetLockType;
        Property: CraftingPropertyType;
    };
    Property?: Calculate<ExtendedItemProperties>;
}

export type OutfitItemMapType = Map<AssetGroupName, OutfitItemType>;

export function buildOutfitItemMap(src: OutfitItemType[]) {
    return new Map(src.map(_ => [_.Asset.Group as AssetGroupName, _]));
}

export function filterOutfitItemMap(src: Map<AssetGroupName, OutfitItemType>, group: AssetGroupName[]) {
    return new Map(group.map(_ => [_, src.get(_)!]));
}

export function buildAppMap(src: Character) {
    return new Map(src.Appearance.map(_ => [_.Asset.Group.Name, _]));
}

export function buildItemsMap(src: Character) {
    return new Map(src.Appearance.filter(_ => _.Asset.Group.Category === "Item").map(_ => [_.Asset.Group.Name as AssetGroupItemName, _]));
}

type CheckOutfitItemOption = {
    group?: boolean;
    craft?: boolean | { MemberNumber: number, MemberName: string };
    lock?: AssetLockType | boolean;
}

export function CheckOutfitItem(V: Item | undefined, E: OutfitItemType | undefined, option?: CheckOutfitItemOption) {
    if (!V || !E) return false;
    if (V.Asset.Name !== E.Asset.Name) return false;
    if (V.Asset.AllowLock) {
        if (!V.Property) return false;
        if (option?.lock === true && !V.Property?.LockedBy) return false;
        if (typeof option?.lock === "string" && V.Property?.LockedBy !== option.lock) return false;
    }
    if (option?.group && V.Asset.Group.Name !== E.Asset.Group) return false;
    if (option?.craft) {
        if (typeof option.craft === "boolean") {
            if (!V.Craft || V.Craft.Name !== E.Craft.Name || V.Craft.Description !== E.Craft.Description) return false;
        } else {
            if (!V.Craft || V.Craft.Name !== E.Craft.Name || V.Craft.Description !== E.Craft.Description
                || V.Craft.MemberNumber !== option.craft.MemberNumber || V.Craft.MemberName !== option.craft.MemberName) return false;
        }
    }
    return true;
}

export function CheckOutfitItemCE(C: Character, E: OutfitItemType | undefined, option?: CheckOutfitItemOption) {
    if (!E) return false;
    const iV = C.Appearance.find(e => e.Asset.Group.Name === E.Asset.Group);
    if (!iV) return false;
    return CheckOutfitItem(iV, E, option);
}

export function CheckOutfitCloth(V: Item, E: OutfitClothType | undefined, option?: { color?: boolean }) {
    if (!E) return false;
    if (V.Asset.Name !== E.Asset.Name) return false;
    if (option?.color && !CompareColor(V.Color, Result(E.Color))) return false;
    return true;
}

export function CheckTargetOutfitItems(Target: Character, Es: OutfitItemType[], option?: CheckOutfitItemOption) {
    const app_map = new Map(Target.Appearance.map(_ => [_.Asset.Group.Name, _]));
    return Es.every(E => {
        let V = app_map.get(E.Asset.Group);
        return V && CheckOutfitItem(V, E, option);
    });
}

type OutfitManifestCraftOption = {
    MemberNumber: number;
    MemberName: string;
}

type OutfitManifestLockOption = {
    Lock: AssetLockType;
    MemberNumber?: number;
}

type OutfitItemManifestOption = {
    craft?: OutfitManifestCraftOption;
    lock?: OutfitManifestLockOption;
    difficulty?: number;
}

export function OutfitItemManifest(Wearer: Character, E: OutfitItemType, option?: OutfitItemManifestOption) {
    const Asset = AssetGet(Wearer.AssetFamily, E.Asset.Group, E.Asset.Name);
    if (!Asset) return;

    const Color = Result(E.Color);

    const Craft: CraftingItem | undefined = option?.craft && E.Craft && {
        ...E.Craft,
        MemberNumber: option.craft.MemberNumber,
        MemberName: option.craft.MemberName,
        Item: E.Asset.Name,
        ItemProperty: null,
        Color: typeof Color === "string" ? Color : Color.join(","),
        Lock: E.Craft?.Lock ? E.Craft.Lock : "",
        Private: true,
    };

    let ret: Item = { Asset, Color, Craft }

    ExtendedItemInit(Wearer, ret, false, false);

    ret.Difficulty = option?.difficulty ?? Asset.Difficulty;

    const props = E.Property && Result(E.Property);
    if (props) {
        if (props.TypeRecord) ExtendedItemSetOptionByRecord(Wearer, ret, props.TypeRecord, { push: false });
        if (!ret.Property) ret.Property = {};
        const rProps = ret.Property;
        for (const key in props) {
            const k = key as keyof ItemProperties;
            if (key === "TypeRecord") continue;
            else if (key === "Effect") {
                if (IsStringArray(props.Effect)) {
                    if (IsStringArray(rProps.Effect)) {
                        props.Effect.forEach(e => {
                            if (rProps.Effect && !rProps.Effect.includes(e)) {
                                rProps.Effect.push(e);
                            }
                        });
                    } else {
                        rProps.Effect = Array.from(props.Effect);
                    }
                }
            }
            else if (key === "ExcludeEffect") {
                if (IsStringArray(props.ExcludeEffect) && IsStringArray(rProps.Effect)) {
                    rProps.Effect = rProps.Effect.filter(e => !props.ExcludeEffect?.includes(e));
                }
            }
            else if (rProps[k] === undefined || typeof rProps[k] === typeof props[k]) {
                Object.assign(rProps, { [k]: props[k] });
            }
        }
    }

    if (option?.lock) {
        const lock = AssetGet(Wearer.AssetFamily, "ItemMisc", option.lock.Lock);
        if (lock) InventoryLock(Wearer, ret, { Asset: lock }, option.lock.MemberNumber, false);
    } else if (E.Craft?.Lock) {
        const lock = AssetGet(Wearer.AssetFamily, "ItemMisc", E.Craft.Lock);
        if (lock) InventoryLock(Wearer, ret, { Asset: lock }, null, false);
    }

    return ret;
}

export function OutfitClothManifest(Wearer: Character, E: OutfitClothType) {
    const Asset = AssetGet(Wearer.AssetFamily, E.Asset.Group, E.Asset.Name);
    if (!Asset) return;

    const Color = Result(E.Color);

    let ret: Item = { Asset, Color };

    ExtendedItemInit(Wearer, ret, false, false);

    if (E.Property?.TypeRecord) ExtendedItemSetOptionByRecord(Wearer, ret, E.Property?.TypeRecord, { push: false });
    if (E.Property?.OverridePriority) {
        if (ret.Property) ret.Property.OverridePriority = E.Property.OverridePriority;
    }
    return ret;
}

function CompareCraft(A: CraftingItem | undefined, B: CraftingItem | undefined) {
    if (A === B) return true;
    if (!A || !B) return false;
    return (['Item', 'Description', 'Lock', 'Color', 'Property', 'MemberName', 'MemberNumber'] as (keyof CraftingItem)[])
        .every(key => A[key] === B[key]);
}

function CompareTypeRecord(A: TypeRecord | undefined, B: TypeRecord | undefined) {
    if (A === B) return true;
    if (!A || !B) return false;
    return CompareRecord(A, B);
}

function CompareEffect(A: string[] | undefined, B: string[] | undefined) {
    if (A === B) return true;
    if (!A || !B) return false;
    return A.length === B.length && A.every(e => B.includes(e));
}

export function CompareItem(A: Item, B: Item) {
    for (const key in A) {
        if (key === "Asset") {
            if (A.Asset.Name !== B.Asset.Name || A.Asset.Group.Name !== B.Asset.Group.Name) return false;
        } else if (key === "Color") {
            if (!CompareColor(A.Color, B.Color)) return false;
        } else if (key === "Craft") {
            if (!CompareCraft(A.Craft, B.Craft)) return false;
        } else if (key === "Difficulty") {
            if (A.Difficulty !== B.Difficulty) return false;
        } else if (key === "Property") {
            if (!CompareTypeRecord(A.Property?.TypeRecord, B.Property?.TypeRecord)) return false;
            if (!CompareEffect(A.Property?.Effect, B.Property?.Effect)) return false;
            if (!(['LockedBy', 'Text',
                'Text2', 'Text3'] as (keyof ItemProperties)[]).every(k => A.Property?.[k] === B.Property?.[k])) return false;
        }
    }
    return true;
}