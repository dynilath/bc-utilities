import { ModSDKModAPI } from "bondage-club-mod-sdk";
import { ActivityDeconstruct, ActivityInfo } from "./ChatMessages";
import { AquireGlobal } from "./Global";

type EventType = (player: PlayerCharacter) => void;
type ChatOrWhisperEvent = (player: PlayerCharacter, sender: Character, msg: string, type: "Chat" | "Whisper" | "Emote") => void;
type ActivityEvent = (player: PlayerCharacter, sender: Character, data: ActivityInfo) => void;

export class ChatRoomHandler {
    private playerJoin: EventType[] = [];
    private playerLeave: EventType[] = [];

    private receiveChatOrWhisper: ChatOrWhisperEvent[] = [];
    private receiveActivity: ActivityEvent[] = [];

    private constructor(mod: ModSDKModAPI) {

        mod.hookFunction("ChatRoomSync", 1, (args, next) => {
            if (Player && (!ChatRoomData || !Player.LastChatRoom || ChatRoomData.Name !== Player.LastChatRoom.Name)) {
                const pl = Player;
                this.playerJoin.forEach(f => f(pl));
            }
            return next(args);
        });

        mod.hookFunction("ServerSend", 1, (args, next) => {
            if (args[0] === "ChatRoomLeave") {
                if (Player) {
                    const pl = Player;
                    this.playerLeave.forEach(f => f(pl));
                }
            }
            return next(args);
        });

        mod.callOriginal("ChatRoomRegisterMessageHandler", [this.ChatRoomMsgHandler(mod)]);
    }
    onAfterPlayerJoin(f: EventType) { this.playerJoin.push(f); }
    onBeforePlayerLeave(f: EventType) { this.playerLeave.push(f); }
    onReceiveChatWhisperEmote(f: ChatOrWhisperEvent) { this.receiveChatOrWhisper.push(f); }
    onReceiveActivity(f: ActivityEvent) { this.receiveActivity.push(f); }

    ChatRoomMsgHandler(mod: ModSDKModAPI): ChatRoomMessageHandler {
        return {
            Description: "BC-utilities Message Hook",
            Priority: 90,
            Callback: (data, sender, msg, metadata) => {
                if (Player && Player.MemberNumber) {
                    const pl = Player;
                    if (data.Type === "Chat" || data.Type === "Whisper" || data.Type === "Emote") {
                        const type = data.Type;
                        if (mod.callOriginal("ChatRoomCharacterViewIsActive", []) || mod.callOriginal("ChatRoomMapViewCharacterIsHearable", [sender]))
                            this.receiveChatOrWhisper.forEach(f => f(pl, sender, msg, type));
                    }
                    if (data.Type === "Activity" && data.Dictionary) {
                        const d = ActivityDeconstruct(data.Dictionary);
                        if (d) this.receiveActivity.forEach(f => f(pl, sender, d));
                    }
                }
                return false;
            },
        }
    }

    static init(mod: ModSDKModAPI): Promise<ChatRoomHandler> {
        return AquireGlobal("ChatRoomHandler", () => new ChatRoomHandler(mod));
    }
}