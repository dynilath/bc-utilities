export function AppearanceUpdate(C: Character, ...Group: AssetGroupName[]) {
    if (ChatRoomData) {
        CharacterRefresh(C, false);
        if (Group.length > 0) Group.forEach(G => { ChatRoomCharacterItemUpdate(C, G); });
        else ChatRoomCharacterUpdate(C);
    } else {
        CharacterRefresh(C, true);
    }
}