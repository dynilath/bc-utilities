function isTargetCharacter(v: ChatMessageDictionaryEntry): v is TargetCharacterDictionaryEntry {
    return typeof (v as Partial<TargetCharacterDictionaryEntry>).TargetCharacter === "number";
}

function isSourceCharacter(v: ChatMessageDictionaryEntry): v is SourceCharacterDictionaryEntry {
    return typeof (v as Partial<SourceCharacterDictionaryEntry>).SourceCharacter === "number";
}

function isActivityName(v: ChatMessageDictionaryEntry): v is ActivityNameDictionaryEntry {
    return typeof (v as Partial<ActivityNameDictionaryEntry>).ActivityName === "string";
}

function isFocusAssetGroup(v: ChatMessageDictionaryEntry): v is FocusGroupDictionaryEntry {
    return typeof (v as Partial<FocusGroupDictionaryEntry>).FocusGroupName === "string";
}

function isActivityAsset(v: ChatMessageDictionaryEntry): v is ActivityAssetReferenceDictionaryEntry {
    return (v as Partial<ActivityAssetReferenceDictionaryEntry>).Tag === "ActivityAsset";
}

export function ActivityDeconstruct(dict: ChatMessageDictionary): ActivityInfo | undefined {
    const ret = dict.reduce((pv, cv) => {
        if (isTargetCharacter(cv)) pv.TargetCharacter = cv.TargetCharacter;
        else if (isSourceCharacter(cv)) pv.SourceCharacter = cv.SourceCharacter;
        else if (isActivityName(cv)) pv.ActivityName = cv.ActivityName;
        else if (isFocusAssetGroup(cv)) pv.ActivityGroup = cv.FocusGroupName;
        else if (isActivityAsset(cv)) {
            pv.Asset = {
                AssetName: cv.AssetName,
                GroupName: cv.GroupName,
                CraftName: cv.CraftName || "",
            };
        }
        return pv;
    }, ({ BCDictionary: dict } as Partial<ActivityInfo>));

    return [ret.TargetCharacter, ret.SourceCharacter, ret.ActivityName, ret.ActivityGroup].some((x) => x === undefined)
        ? undefined
        : ret as ActivityInfo;
}

export interface ActivityInfo {
    SourceCharacter: number;
    TargetCharacter: number;
    ActivityGroup: AssetGroupName;
    ActivityName: string;
    Asset?: {
        AssetName: string;
        CraftName: string;
        GroupName: AssetGroupName;
    };
    BCDictionary: ChatMessageDictionaryEntry[];
}

export class ChatRoomAction {
    private static _instance: ChatRoomAction | undefined = undefined;

    static get instance(): ChatRoomAction {
        return ChatRoomAction._instance as ChatRoomAction;
    }

    static init(tag: string) {
        if (ChatRoomAction._instance) return;
        ChatRoomAction._instance = new ChatRoomAction(tag);
    }

    public readonly LocalAction: (Content: string) => void;
    public readonly LocalInfo: (Content: string) => void;
    public readonly SendAction: (Content: string) => void;
    public readonly SendChat: (Content: string) => void;
    public readonly SendWhisper: (target: number, Content: string) => void;
    public readonly SendBeep: (target: number, Content: string) => void;

    constructor(readonly CUSTOM_ACTION_TAG: string) {
        const DictItem = (Content: string) => { return { Tag: `MISSING TEXT IN "Interface.csv": ${CUSTOM_ACTION_TAG}`, Text: Content } };

        this.SendAction = (Content: string) => {
            if (!Content || !Player || !Player.MemberNumber) return;
            ServerSend("ChatRoomChat", {
                Content: CUSTOM_ACTION_TAG,
                Type: "Action",
                Dictionary: [DictItem(Content)]
            });
        }

        this.SendChat = (Content: string) => {
            if (!Content || !Player || !Player.MemberNumber) return;
            ServerSend("ChatRoomChat", {
                Content: Content,
                Type: "Chat"
            });
        }

        this.LocalAction = (Content: string) => {
            if (!Content || !Player || !Player.MemberNumber) return;
            ChatRoomMessage({
                Sender: Player.MemberNumber,
                Content: CUSTOM_ACTION_TAG,
                Type: "Action",
                Dictionary: [DictItem(Content)],
            });
        }

        this.LocalInfo = (Content: string) => {
            if (!Content || !Player || !Player.MemberNumber) return;
            ChatRoomMessage({
                Sender: Player.MemberNumber,
                Content: Content,
                Type: "LocalMessage",
            });
        }

        this.SendWhisper = (target: number, Content: string) => {
            if (!Content || !Player || !Player.MemberNumber) return;
            ServerSend("ChatRoomChat", {
                Content: Content,
                Type: "Whisper",
                Target: target
            });
        }

        this.SendBeep = (target: number, Content: string) => {
            if (!Content || !Player || !Player.MemberNumber) return;
            ServerSend("AccountBeep", {
                MemberNumber: target,
                Message: Content,
                BeepType: "",
                IsSecret: false
            });
        }
    }
}