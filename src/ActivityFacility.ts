import { ActivityDeconstruct, ActivityInfo } from "./ChatMessages";
import { ModSDKModAPI } from "bondage-club-mod-sdk";

export type ActivityTriggerMode = "onself" | "selfonother" | "both";

interface ActivityInvokeBase {
    mode: ActivityTriggerMode;
    onBodyparts: AssetGroupItemName[] | undefined;
    on(player: PlayerCharacter, sender: Character, info: ActivityInfo): void;
    adjustDict(Content: string, dict: ChatMessageDictionaryEntry[]): ChatMessageDictionaryEntry[];

    init(mod: ModSDKModAPI): void;
}

export type ExtendedActivityName<Acts> = Acts | ActivityName;
export type ExtendedActivityPrerequisite<Prereqs> = Prereqs | ActivityPrerequisite;

export type ExtendedBCActivity<Acts, Prereqs> = Omit<Activity, "Name" | "Prerequisite" | "ActivityID"> & {
    Name: ExtendedActivityName<Acts>;
    Prerequisite: ExtendedActivityPrerequisite<Prereqs>[];
};

export type CustomBCActivity<Acts, Prereqs> = Omit<Activity, "Name" | "Prerequisite" | "ActivityID"> & {
    Name: Acts;
    Prerequisite: ExtendedActivityPrerequisite<Prereqs>[];
};

export abstract class IActivityCustom<Acts, Prereqs> implements ActivityInvokeBase {
    abstract on(player: PlayerCharacter, sender: Character, info: ActivityInfo): void;
    adjustDict(Content: string, dict: ChatMessageDictionaryEntry[]): ChatMessageDictionaryEntry[] { return dict; };
    abstract text(keyword: string): string;
    init(mod: ModSDKModAPI): void { };
    constructor(
        readonly mode: ActivityTriggerMode,
        readonly onBodyparts: AssetGroupItemName[] | undefined,
        readonly activity: CustomBCActivity<Acts, Prereqs>,
        readonly image: string
    ) { }
}

export abstract class IActivityExtended<Acts, Prereqs> implements ActivityInvokeBase {
    abstract on(player: PlayerCharacter, sender: Character, info: ActivityInfo): void;
    adjustDict(Content: string, dict: ChatMessageDictionaryEntry[]): ChatMessageDictionaryEntry[] { return dict; };
    init(mod: ModSDKModAPI): void { };
    constructor(
        readonly mode: ActivityTriggerMode,
        readonly onBodyparts: AssetGroupItemName[] | undefined,
        readonly activity: ExtendedActivityName<Acts>
    ) { }
}

export type IActivityInvokable<Acts, Prereqs> = IActivityCustom<Acts, Prereqs> | IActivityExtended<Acts, Prereqs>;

export abstract class IActivityPrerequisite<Prereqs> {
    abstract name: Prereqs;
    abstract test(acting: Character, acted: Character, group: AssetGroup): boolean;
}

export class ActivityFacility<Acts, Prereqs> {
    prereqMap = new Map<Prereqs, IActivityPrerequisite<Prereqs>>();
    customActMap = new Map<Acts, IActivityCustom<Acts, Prereqs>>();
    actHandlers = new Map<ExtendedActivityName<Acts>, IActivityInvokable<Acts, Prereqs>[]>();

    constructor(protected mod: ModSDKModAPI) {
        mod.hookFunction("ActivityCheckPrerequisite", 1, (args, next) => {
            const [prereq, acting, acted, group] = args as [string, Character, Character, AssetGroup];
            const cusPrereq = this.prereqMap.get(prereq as Prereqs);
            if (cusPrereq) return cusPrereq.test(acting, acted, group);
            return next(args);
        });

        mod.hookFunction("DrawGetImage", 1, (args, next) => {
            const values = (args[0] as string).split("/");
            if (values[2] === "Activity") {
                const act = this.customActMap.get(values[3].split(".")[0] as Acts);
                if (act) return next([act.image]);
            }
            return next(args);
        });

        mod.hookFunction("ActivityDictionaryText", 1, (args, next) => {
            const splits = (args[0] as string).split("-");
            const activity_name = splits[splits.length - 1] as Acts;
            return this.customActMap.get(activity_name)?.text(args[0]) ?? next(args);
        })

        mod.hookFunction("ServerSend", 1, (args, next) => {
            if (args[0] !== "ChatRoomChat" || args[1]?.Type !== "Activity") return next(args);
            const dict = args[1].Dictionary;
            if (!dict) return next(args);
            const d = ActivityDeconstruct(dict);
            if (!d) return next(args);

            const myact = this.actHandlers.get(d.ActivityName as ExtendedActivityName<Acts>);
            if (!myact) return next(args);
            args[1].Dictionary = myact.reduce((acc, act) => act.adjustDict(args[1].Content, acc), dict);
            return next(args);
        });
    }

    run(player: PlayerCharacter, sender: Character, info: ActivityInfo) {
        this.actHandlers.get(info.ActivityName as ExtendedActivityName<Acts>)?.forEach(h => {
            if (h.onBodyparts !== undefined && !h.onBodyparts.includes(info.ActivityGroup as AssetGroupItemName)) return;
            if (h.mode === "onself" && info.TargetCharacter !== player.MemberNumber) return;
            if (h.mode === "selfonother"
                && (info.TargetCharacter === player.MemberNumber
                    || info.SourceCharacter !== player.MemberNumber)) return;
            h.on(player, sender, info);
        });
    }

    addPrerequisite(prereq: IActivityPrerequisite<Prereqs>) {
        this.prereqMap.set(prereq.name, prereq);
    }

    addActivity(act: IActivityCustom<Acts, Prereqs>) {
        this.customActMap.set(act.activity.Name as Acts, act);
        ActivityFemale3DCG?.push(act.activity as unknown as Activity);
        ActivityFemale3DCGOrdering?.push(act.activity.Name as ActivityName);

        const acts = this.actHandlers.get(act.activity.Name);
        if (acts) acts.push(act);
        else this.actHandlers.set(act.activity.Name, [act]);

        act.init(this.mod);
    }

    addActivityExtension(act: IActivityExtended<Acts, Prereqs>) {
        const acts = this.actHandlers.get(act.activity);
        if (acts) acts.push(act);
        else this.actHandlers.set(act.activity, [act]);

        act.init(this.mod);
    }
}