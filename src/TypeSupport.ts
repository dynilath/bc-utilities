type NotFunction<T> = T extends Function ? never : T;

export type Calculate<T> = NotFunction<T> | (() => NotFunction<T>);

export function Result<T>(cal: Calculate<T>): NotFunction<T> {
    return cal instanceof Function ? cal() : cal;
}

export function IsStringArray(v: any): v is string[] {
    return Array.isArray(v) && v.every(i => typeof i === "string");
}

export function CompareColor(a: ItemColor | undefined, b: ItemColor | undefined) {
    if (a === b) return true;
    if (!a || !b) return false;
    if (a.length !== b.length) return false;
    return (a as string[]).every((v, i) => v === b[i]);
}

export function CompareRecord<T extends Record<string, any>>(a: T, b: T) {
    return Object.keys(a).every(k => a[k] === b[k]);
}