import { ModSDKModAPI } from "bondage-club-mod-sdk";
import { AquireGlobal } from "./Global";

type OrgasmEvent = (player: PlayerCharacter) => void;

type OrgasmEventType = 'Orgasm' | 'Resist' | 'Ruined';

interface Window {
    BCUtilities?: {
        OrgasmMonitor?: () => OrgasmMonitor;
    }
}

export class OrgasmMonitor {
    private readonly events: Record<OrgasmEventType, OrgasmEvent[]> = {
        Orgasm: [],
        Resist: [],
        Ruined: []
    }

    private constructor(mod: ModSDKModAPI) {
        mod.hookFunction('ActivityOrgasmStop', 9, (args, next) => {
            const C = args[0] as Character;
            const Progress = args[1] as number;
            if (C.IsPlayer()) {
                if (ActivityOrgasmRuined) this.events.Ruined.forEach(_ => _(C));
                // ActivityOrgasmStop(Player, 70) when the player resisted
                else if (Progress >= 60) this.events.Resist.forEach(_ => _(C));
            }
            next(args);
        });

        mod.hookFunction('ActivityOrgasmStart', 9, (args, next) => {
            const C = args[0] as Character;
            if (C.IsPlayer() && !ActivityOrgasmRuined) this.events.Orgasm.forEach(_ => _(C));
            next(args);
        });
    }

    public onOrgasm(event: OrgasmEvent) {
        this.events.Orgasm.push(event);
    }

    public onResist(event: OrgasmEvent) {
        this.events.Resist.push(event);
    }

    public onRuined(event: OrgasmEvent) {
        this.events.Ruined.push(event);
    }

    public on(event: OrgasmEventType, callback: OrgasmEvent) {
        this.events[event]?.push(callback);
    }

    static init(mod: ModSDKModAPI): Promise<OrgasmMonitor> {
        return AquireGlobal("OrgasmMonitor", () => new OrgasmMonitor(mod));
    }
}