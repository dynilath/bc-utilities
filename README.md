# BC Modding Utilities

This package contains a collection of utilities for modding in the Bondage Club.

## How to use

Make a pnpm workspace containing your mod and this package. Add `workspace:bc-utilities` to the `dependencies` section of your mod's `package.json` and run `pnpm install`. You can now import the utilities from this package in your mod.

## Use shared global types

After `pnpm install`. Create a *club.d.ts* with the following content:

```typescript
/// <reference types="bc-utilities" />
```

Now you can use the shared global types in your mod.